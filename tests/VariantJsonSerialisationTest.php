<?php

namespace Rodium\Core\Catalog\Api;

use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;
use Rodium\Core\Catalog\Api\Variant\Dimensions;
use Rodium\Core\Catalog\Api\Variant\Photo;
use Rodium\Core\Catalog\Api\Variant\RetailPrice;
use Rodium\Core\Catalog\Api\Variant\RodiumId;
use Rodium\Core\Catalog\Api\Variant\Symbol;
use Rodium\Core\Catalog\Api\Variant\Variant;
use Rodium\Core\Catalog\Api\Variant\VariantId;

class VariantJsonSerialisationTest extends TestCase
{
    /** @var Serializer */
    private $serializer;

    public static function setUpBeforeClass()
    {
        $loader = include __DIR__.'/../vendor/autoload.php';
        AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
    }

    protected function setUp()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @test
     * @dataProvider variants
     * @param Variant $variant
     * @param string $expectedJson
     */
    public function itSerialisesVariantToJson(Variant $variant, $expectedJson)
    {
        $serialised = $this->serializer->serialize($variant, 'json');
        $this->assertEquals($expectedJson, $serialised);

    }

    /**
     * @test
     * @dataProvider variants
     * @param Variant $variant
     * @param string $inputJson
     */
    public function itDeserialisesJsonToVariant(Variant $variant, $inputJson)
    {
        $this->assertEquals(
            $variant,
            $this->serializer->deserialize($inputJson, 'Rodium\Core\Catalog\Api\Variant\Variant', 'json')
        );
    }

    public function variants()
    {
        return [
            [
                new Variant(
                    $variantId = VariantId::parse(123),
                    $symbol = new Symbol("Pk214"),
                    $rodiumId = RodiumId::parse(321),
                    $dimensions = new Dimensions(
                        2.35,
                        32.5,
                        23.7,
                        12.1,
                        10.4
                    ),
                    $itemUrl = "http://prezenty/katalog/pierscionk/Pk214",
                    $price = new RetailPrice(
                        245.12, 23.0
                    ),
                    [
                        $photo1 = new Photo(
                            "http://prezenty/katalog/pierscionk/Pk214-thumb.jpg",
                            "http://prezenty/katalog/pierscionk/Pk214-full.jpg"
                        ),
                        $photo2 = new Photo(
                            "http://prezenty/katalog/pierscionk/Pk214-thumb2.jpg",
                            "http://prezenty/katalog/pierscionk/Pk214-full2.jpg"
                        )
                    ],
                    $youToubeId = "2134rtefdqw32"
                ),
                json_encode([
                    'id' => $variantId->id(),
                    'symbol' => (string)$symbol,
                    'rodium_id' => $rodiumId->id(),
                    'dimensions' => [
                        'mass' => $dimensions->mass(),
                        'width' => $dimensions->width(),
                        'length' => $dimensions->length(),
                        'height' => $dimensions->height(),
                        'diameter' => $dimensions->diameter()
                    ],
                    'price' => [
                        'tax_percent' => $price->taxPercent(),
                        'gross' => $price->gross(),
                        'type' => 'retail'
                    ],
                    'item_url' => $itemUrl,
                    'photos' => [
                        [
                            'thumb' => $photo1->thumb(),
                            'full' => $photo1->full()
                        ],
                        [
                            'thumb' => $photo2->thumb(),
                            'full' => $photo2->full()
                        ]
                    ],
                    'you_tube_id' => $youToubeId
                ])
            ]
        ];
    }
}
