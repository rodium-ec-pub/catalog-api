<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use PHPUnit\Framework\TestCase;
use Rodium\Core\Catalog\Api\Auth\BasicAuth;

class RemoteVariantApiFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCreatesRemoteVariantApiFactory()
    {
        $variantApi = RemoteVariantApiFactory::create('base-url');
        $this->assertInstanceOf(RemoteVariantApi::class, $variantApi);
    }

    /**
     * @test
     */
    public function itCreatesRemoteVariantApiFactoryWithAuth()
    {
        $variantApi = RemoteVariantApiFactory::create('base-url', new BasicAuth('user', 'pass'));
        $this->assertInstanceOf(RemoteVariantApi::class, $variantApi);
    }
}