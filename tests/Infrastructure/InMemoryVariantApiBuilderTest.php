<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use PHPUnit\Framework\TestCase;
use Rodium\Core\Catalog\Api\Variant\VariantId;

class InMemoryVariantApiBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function itAddsDefaultVariants()
    {
        $builder = new InMemoryVariantApiBuilder();
        $builder->addDefaultVariants();

        $variantApi = $builder->build();

        $variant = $variantApi->variantOfId($variantId = VariantId::parse(23841));
        $this->assertInstanceOf('Rodium\Core\Catalog\Api\Variant\Variant', $variant);
        $this->assertEquals($variantId, $variant->id());

        $variant = $variantApi->variantOfId($variantId = VariantId::parse(6755));
        $this->assertInstanceOf('Rodium\Core\Catalog\Api\Variant\Variant', $variant);
        $this->assertEquals($variantId, $variant->id());
    }
}
