<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use PHPUnit\Framework\TestCase;
use Rodium\Core\Catalog\Api\Auth\BasicAuth;
use Rodium\Core\Catalog\Api\Variant\Variant;
use Rodium\Core\Catalog\Api\Variant\VariantId;

class RemoteVariantApiIntegrationTest extends TestCase
{
    /** @var RemoteVariantApi */
    private $variantApi;

    protected function setUp()
    {
        $baseUrl = getenv('api.base_url');
        if (!$baseUrl) {
            $this->markTestSkipped('Set "api.base_url" in your phpunit.xml');
        }

        $auth = $this->getBasicAuth();
        $this->variantApi = RemoteVariantApiFactory::create($baseUrl, $auth);
    }

    /**
     * @test
     */
    public function itGetsVariantById()
    {
        $variant = $this->variantApi->variantOfId(VariantId::parse(1725));
        $this->assertInstanceOf(Variant::class, $variant);
    }

    private function getBasicAuth()
    {
        $user = getenv('api.basic_auth.user');
        $password = getenv('api.basic_auth.password');

        if (!($user && $password)) {
            return null;
        }

        return new BasicAuth($user, $password);
    }
}