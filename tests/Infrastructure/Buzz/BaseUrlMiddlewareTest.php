<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure\Buzz;

use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class BaseUrlMiddlewareTest extends TestCase
{
    /**
     * @test
     * @dataProvider urls
     */
    public function itAppendsBaseUrl($baseUrl, $path, $expectedUrl)
    {
        $middleware = new BaseUrlMiddleware($baseUrl);

        $request = new \GuzzleHttp\Psr7\Request('GET', $path);

        /** @var Request $newRequest */
        $newRequest = $middleware->handleRequest($request, function ($r) {return $r;});

        $this->assertEquals($expectedUrl, (string)$newRequest->getUri());
    }

    public function urls()
    {
        return [
            [
                'http://host.com/base/',
                '/api/somePath?query=243',
                'http://host.com/base/api/somePath?query=243'
            ]
        ];
    }
}

