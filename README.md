# Rodium Catalog API

## Installation

Add to your **composer.json**

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "ssh://git@gitlab.com:rodium-ec-pub/catalog-api.git"
        }
    ],
    "require": {
        "rodium-core/catalog-api": "^1.0.0"
    },
    "minimum-stability": "dev",
    "prefer-stable": true
}
```

## VariantApi

**VariantApi** interface supports:

 * getting variant by ID: $variantApi->variantOfId(VariantId::parse(123)); // returns Variant

### RemoteVariantApi

Please note, **RemoteVariantApi** requires AnnotationRegistry to be configured.
You can add the following lines to your **autoload.php**

```php
use Doctrine\Common\Annotations\AnnotationRegistry;
$loader = include __DIR__.'/vendor/autoload.php';
AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
```

Create and instance of **RemoteVariantApi**

```php
use \Rodium\Core\Catalog\Api\Variant\Infrastructure\RemoteVariantApiFactory;
use \Rodium\Core\Catalog\Api\Auth\BasicAuth;

$auth = new BasicAuth('username', 'password'); // if required
$variantApi = RemoteVariantApiFactory::create('http://base-url/to/api', $auth);
```

### InMemoryVariantApi

In order to test the integration locally, **InMemoryVariantApi** can be used.

```php
use Rodium\Core\Catalog\Api\Variant\Infrastructure\InMemoryVariantApiBuilder;
use Rodium\Core\Catalog\Api\Variant\VariantId;
use Rodium\Core\Catalog\Api\Variant\Variant;

$builder = new InMemoryVariantApiBuilder();

$builder->addDefaultVariants(); // adds Variants of ID "23841" and "6755"

// optionally add your own Variants for testing purpose 
$builder->addVariant(
    new Variant(...)
);

$variantApi = $builder->build();

$variantApi->variantOfId(VariantId::parse(23841)); // returns Variant 23841
$variantApi->variantOfId(VariantId::parse(6755)); // returns Variant 6755
```
