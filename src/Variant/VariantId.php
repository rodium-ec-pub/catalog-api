<?php

namespace Rodium\Core\Catalog\Api\Variant;

final class VariantId extends AbstractId
{
    /**
     * @param int $id
     * @return VariantId
     */
    public static function parse($id)
    {
        return new VariantId((int)$id);
    }
}
