<?php

namespace Rodium\Core\Catalog\Api\Variant;

use JMS\Serializer\Annotation as JMS;

final class RetailPrice extends AbstractPrice
{
    /**
     * @var float
     * @JMS\Type("double")
     */
    private $gross;

    /**
     * @param float $gross
     * @param float $taxPercent
     */
    public function __construct($gross, $taxPercent)
    {
        parent::__construct($taxPercent);
        $this->gross = $gross;
    }

    /**
     * @return float
     */
    public function net()
    {
        return $this->gross() / ($this->taxPercent() + 100) / 100;
    }

    /**
     * @return float
     */
    public function gross()
    {
        return $this->gross;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return (string)$this->gross();
    }
}
