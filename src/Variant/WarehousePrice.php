<?php

namespace Rodium\Core\Catalog\Api\Variant;

use JMS\Serializer\Annotation as JMS;

final class WarehousePrice extends AbstractPrice
{
    /**
     * @var float
     * @JMS\Type("double")
     */
    private $net;

    /**
     * @param float $net
     * @param float $taxPercent
     */
    public function __construct($net, $taxPercent)
    {
        parent::__construct($taxPercent);
        $this->net = $net;
    }

    /**
     * @return float
     */
    public function net()
    {
        return $this->net;
    }

    /**
     * @return float
     */
    public function gross()
    {
        return $this->net() * ($this->taxPercent() + 100) / 100;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return (string)$this->net();
    }
}