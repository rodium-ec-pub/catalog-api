<?php

namespace Rodium\Core\Catalog\Api\Variant;

use JMS\Serializer\Annotation as JMS;

final class Variant
{
    /**
     * @var int
     * @JMS\Type("int")
     */
    private $id;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $symbol;

    /**
     * @var int
     * @JMS\Type("int")
     */
    private $rodiumId;

    /**
     * @var Dimensions
     * @JMS\Type("Rodium\Core\Catalog\Api\Variant\Dimensions")
     */
    private $dimensions;

    /**
     * @var Price
     * @JMS\Type("Rodium\Core\Catalog\Api\Variant\AbstractPrice")
     */
    private $price;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $itemUrl;

    /**
     * @var Photo[]
     * @JMS\Type("array<Rodium\Core\Catalog\Api\Variant\Photo>")
     */
    private $photos;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $youTubeId;

    public function __construct(
        VariantId $id,
        Symbol $symbol,
        RodiumId $rodiumId,
        Dimensions $dimensions,
        $itemUrl,
        Price $price,
        array $photos = [],
        $youTubeId = null
    ) {
        $this->id = $id->id();
        $this->symbol = (string)$symbol;
        $this->rodiumId = $rodiumId->id();
        $this->dimensions = $dimensions;
        $this->itemUrl = $itemUrl;
        $this->price = $price;
        $this->photos = $photos ?: [];
        $this->youTubeId = $youTubeId;
    }

    /**
     * @return VariantId
     */
    public function id()
    {
        return VariantId::parse($this->id);
    }

    /**
     * @return Symbol
     */
    public function symbol()
    {
        return new Symbol($this->symbol);
    }

    /**
     * @return RodiumId
     */
    public function rodiumId()
    {
        return RodiumId::parse($this->rodiumId);
    }

    /**
     * @return Dimensions
     */
    public function dimensions()
    {
        return $this->dimensions;
    }

    /**
     * @return Price
     */
    public function price()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function itemUrl()
    {
        return $this->itemUrl;
    }

    /**
     * @return Photo|null
     */
    public function mainPhoto()
    {
        if ($photos = $this->photos) {
            return array_shift($photos);
        }

        return null;
    }

    /**
     * @return Photo[]
     */
    public function photos()
    {
        return $this->photos;
    }

    /**
     * @return string
     */
    public function youTubeId()
    {
        return $this->youTubeId;
    }
}
