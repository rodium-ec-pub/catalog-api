<?php

namespace Rodium\Core\Catalog\Api\Variant;

use JMS\Serializer\Annotation as JMS;

final class Dimensions
{
    /**
     * @var float
     * @JMS\Type("double")
     */
    private $mass;

    /**
     * @var float
     * @JMS\Type("double")
     */
    private $width;

    /**
     * @var float
     * @JMS\Type("double")
     */
    private $length;

    /**
     * @var float
     * @JMS\Type("double")
     */
    private $height;

    /**
     * @var float
     * @JMS\Type("double")
     */
    private $diameter;

    /**
     * @param float|null $mass
     * @param float|null $width
     * @param float|null $length
     * @param float|null $height
     * @param float|null $diameter
     */
    public function __construct($mass, $width, $length, $height, $diameter)
    {
        $this->mass = $mass;
        $this->width = $width;
        $this->length = $length;
        $this->height = $height;
        $this->diameter = $diameter;
    }

    public function mass()
    {
        return $this->mass;
    }

    public function width()
    {
        return $this->width;
    }

    public function length()
    {
        return $this->length;
    }

    public function height()
    {
        return $this->height;
    }

    public function diameter()
    {
        return $this->diameter;
    }
}