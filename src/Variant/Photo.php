<?php

namespace Rodium\Core\Catalog\Api\Variant;

use JMS\Serializer\Annotation as JMS;

final class Photo
{
    /**
     * @var string
     * @JMS\Type("string")
     */
    private $thumb;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $full;

    /**
     * @param string $thumb
     * @param string $full
     */
    public function __construct($thumb, $full)
    {
        $this->thumb = $thumb;
        $this->full = $full;
    }

    /**
     * @return string
     */
    public function thumb()
    {
        return $this->thumb;
    }

    /**
     * @return string
     */
    public function full()
    {
        return $this->full;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->full();
    }
}