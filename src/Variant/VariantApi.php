<?php

namespace Rodium\Core\Catalog\Api\Variant;

interface VariantApi
{
    /**
     * @param VariantId $id
     * @return Variant
     * @throws VariantNotFoundException
     */
    public function variantOfId(VariantId $id);
}
