<?php

namespace Rodium\Core\Catalog\Api\Variant;

class VariantNotFoundException extends VariantApiException
{
    /** @var VariantId */
    private $variantId;

    public function __construct(VariantId $id, \Exception $previous = null)
    {
        parent::__construct(sprintf('Could not find the variant of ID "%s".', $id), 0, $previous);
        $this->variantId = $id;
    }

    /**
     * @param VariantId $id
     * @param \Exception|null $previous
     * @return VariantNotFoundException
     */
    public static function forVariantId(VariantId $id, \Exception $previous = null)
    {
        return new self($id, $previous);
    }

    /**
     * @return VariantId
     */
    public function variantId()
    {
        return $this->variantId;
    }
}