<?php

namespace Rodium\Core\Catalog\Api\Variant;

interface Price
{
    /**
     * @return float
     */
    public function net();

    /**
     * @return float
     */
    public function gross();

    /**
     * @return float
     */
    public function taxPercent();

    /**
     * @inheritdoc
     */
    public function __toString();
}
