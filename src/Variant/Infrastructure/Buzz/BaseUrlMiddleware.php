<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure\Buzz;

use Buzz\Middleware\MiddlewareInterface;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class BaseUrlMiddleware implements MiddlewareInterface
{
    /** @var Uri */
    private $baseUrl;

    public function __construct($baseUrl)
    {
        $this->baseUrl = new Uri($baseUrl);
    }

    /**
     * @inheritdoc
     */
    public function handleRequest(RequestInterface $request, callable $next)
    {
        return $next(
            $request->withUri(
                $request->getUri()
                    ->withHost($this->baseUrl->getHost())
                    ->withScheme($this->baseUrl->getScheme())
                    ->withPath(
                        sprintf(
                            '%s/%s',
                            trim($this->baseUrl->getPath(), '/'),
                            trim($request->getUri()->getPath(), '/')
                        )
                    )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function handleResponse(RequestInterface $request, ResponseInterface $response, callable $next)
    {
        return $next($request, $response);
    }
}