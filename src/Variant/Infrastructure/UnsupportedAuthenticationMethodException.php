<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use Rodium\Core\Catalog\Api\Auth\Auth;

class UnsupportedAuthenticationMethodException extends \InvalidArgumentException
{
    public static function forAuth(Auth $auth)
    {
        return new self(
            sprintf('Authentication method of class "%s" is not supported.', get_class($auth))
        );
    }
}