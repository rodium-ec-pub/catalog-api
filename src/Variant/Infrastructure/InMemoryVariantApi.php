<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use Rodium\Core\Catalog\Api\Variant\Variant;
use Rodium\Core\Catalog\Api\Variant\VariantApi;
use Rodium\Core\Catalog\Api\Variant\VariantId;
use Rodium\Core\Catalog\Api\Variant\VariantNotFoundException;

final class InMemoryVariantApi implements VariantApi
{
    /** @var Variant[] */
    private $variants;

    /**
     * @param Variant[] $variants
     */
    public function __construct(array $variants)
    {
        $this->variants = array();
        foreach ($variants as $variant) {
            $this->addVariant($variant);
        }
    }

    private function addVariant(Variant $variant)
    {
        $this->variants[(string)$variant->id()] = $variant;
    }

    public function variantOfId(VariantId $id)
    {
        if (isset($this->variants[(string)$id])) {
            return $this->variants[(string)$id];
        }

        throw VariantNotFoundException::forVariantId($id);
    }
}
