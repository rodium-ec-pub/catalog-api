<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use Rodium\Core\Catalog\Api\Variant\VariantApiException;
use Rodium\Core\Catalog\Api\Variant\VariantId;

class VariantRemoteApiException extends VariantApiException
{
    /** @var VariantId */
    private $variantId;

    /**
     * @param VariantId $id
     * @param \Exception $e
     * @return VariantRemoteApiException
     */
    public static function forGettingVariantById(VariantId $id, \Exception $e = null)
    {
        return new self(
            sprintf('Error during getting Variant of ID "%s".', $id),
            0,
            $e
        );
    }

    /**
     * @return VariantId
     */
    public function variantId()
    {
        return $this->variantId;
    }
}
