<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use Rodium\Core\Catalog\Api\Variant\Dimensions;
use Rodium\Core\Catalog\Api\Variant\Photo;
use Rodium\Core\Catalog\Api\Variant\RetailPrice;
use Rodium\Core\Catalog\Api\Variant\RodiumId;
use Rodium\Core\Catalog\Api\Variant\Symbol;
use Rodium\Core\Catalog\Api\Variant\Variant;
use Rodium\Core\Catalog\Api\Variant\VariantId;

final class InMemoryVariantApiBuilder
{
    /** @var Variant[] */
    private $variants;

    public function __construct()
    {
        $this->variants = array();
    }

    public function addDefaultVariants()
    {
        $this->variants[] = new Variant(
            VariantId::parse(23841),
            new Symbol("Pi005"),
            RodiumId::parse(46144),
            new Dimensions(
                1.40,
                null,
                null,
                null,
                15
            ),
            "http://katalog.sklepjubilerski.pl/katalog/pierscionki/tradycyjne/pi005",
            new RetailPrice(
                430.00,
                23
            ),
            array(
                new Photo(
                    "http://katalog.sklepjubilerski.pl/media/catalog/items/catalog_item_thumb/a/a/f/8099cee240b752a33ae36f3f41451aaf.png",
                    "http://katalog.sklepjubilerski.pl/media/catalog/items/catalog_item_xxl/a/a/f/8099cee240b752a33ae36f3f41451aaf.png"
                )
            )
        );

        $this->variants[] = new Variant(
            VariantId::parse(6755),
            new Symbol("Lp022"),
            RodiumId::parse(28665),
            new Dimensions(
                1.35,
                null,
                50,
                null,
                null
            ),
            "http://katalog.sklepjubilerski.pl/katalog/lancuszki/pelne/singapore/lp022",
            new RetailPrice(
                400.00,
                23
            ),
            array(
                new Photo(
                    "http://katalog.sklepjubilerski.pl/media/catalog/items/catalog_item_thumb/2/8/7/217690516f21eedd97428ac685973287.png",
                    "http://katalog.sklepjubilerski.pl/media/catalog/items/catalog_item_xxl/2/8/7/217690516f21eedd97428ac685973287.png"
                ),
                new Photo(
                    "http://katalog.sklepjubilerski.pl/media/catalog/items/catalog_item_thumb/8/5/3/f40bca2f190255f29f4c732783901853.png",
                    "http://katalog.sklepjubilerski.pl/media/catalog/items/catalog_item_xxl/8/5/3/f40bca2f190255f29f4c732783901853.png"
                )
            )
        );
    }

    public function addVariant(Variant $variant)
    {
        $this->variants[] = $variant;
    }

    /**
     * @return InMemoryVariantApi
     */
    public function build()
    {
        return new InMemoryVariantApi(
            $this->variants
        );
    }
}
