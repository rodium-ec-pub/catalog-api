<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use Buzz\Browser;
use Buzz\Message\Response;
use JMS\Serializer\SerializerInterface;
use Rodium\Core\Catalog\Api\Variant\VariantApi;
use Rodium\Core\Catalog\Api\Variant\VariantId;
use Rodium\Core\Catalog\Api\Variant\VariantNotFoundException;

final class RemoteVariantApi implements VariantApi
{
    /** @var Browser */
    private $browser;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(Browser $browser, SerializerInterface $serializer) {
        $this->browser = $browser;
        $this->serializer = $serializer;
    }

    /**
     * @inheritdoc
     */
    public function variantOfId(VariantId $id)
    {
        try {
            /** @var Response $response */
            $response = $this->browser->get(sprintf('/variants/%s', $id));

            if ($response->getStatusCode() == 404) {
                throw VariantNotFoundException::forVariantId($id);
            }

            return $this->serializer->deserialize(
                $response->getContent(),
                'Rodium\Core\Catalog\Api\Variant\Variant',
                'json'
            );
        } catch (VariantNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw VariantRemoteApiException::forGettingVariantById($id, $e);
        }
    }
}
