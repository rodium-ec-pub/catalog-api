<?php

namespace Rodium\Core\Catalog\Api\Variant\Infrastructure;

use Buzz\Browser;
use Buzz\Client\Curl;
use Buzz\Middleware\BasicAuthMiddleware;
use JMS\Serializer\SerializerBuilder;
use Rodium\Core\Catalog\Api\Auth\Auth;
use Rodium\Core\Catalog\Api\Auth\BasicAuth;
use Rodium\Core\Catalog\Api\Variant\Infrastructure\Buzz\BaseUrlMiddleware;

final class RemoteVariantApiFactory
{
    /**
     * @param string $baseUrl
     * @param Auth|null $auth
     * @return RemoteVariantApi
     */
    public static function create($baseUrl, Auth $auth = null)
    {
        $serializer = SerializerBuilder::create()->build();

        $browser = new Browser(new Curl());
        $browser->addMiddleware(new BaseUrlMiddleware($baseUrl));
        if ($auth) {
            self::configureAuth($browser, $auth);
        }

        return new RemoteVariantApi($browser, $serializer);
    }

    private static function configureAuth(Browser $browser, $auth)
    {
        switch (true) {
            case $auth instanceof BasicAuth:
                $browser->addMiddleware(new BasicAuthMiddleware($auth->username(), $auth->password()));
                return;
                break;
        }

        throw UnsupportedAuthenticationMethodException::forAuth($auth);
    }
}
