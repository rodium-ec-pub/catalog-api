<?php

namespace Rodium\Core\Catalog\Api\Variant;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\Discriminator(
 *     field = "type",
 *     disabled = false,
 *     map = {
 *         "retail": "Rodium\Core\Catalog\Api\Variant\RetailPrice",
 *         "warehouse": "Rodium\Core\Catalog\Api\Variant\WarehousePrice"
 *     }
 * )
 */
abstract class AbstractPrice implements Price
{
    /**
     * @var float
     * @JMS\Type("double")
     */
    private $taxPercent;

    /**
     * @param float $taxPercent
     */
    public function __construct($taxPercent)
    {
        $this->taxPercent = $taxPercent;
    }

    /**
     * @inheritdoc
     */
    public function taxPercent()
    {
        return $this->taxPercent;
    }
}
