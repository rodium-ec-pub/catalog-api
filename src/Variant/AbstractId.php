<?php

namespace Rodium\Core\Catalog\Api\Variant;

abstract class AbstractId
{
    /** @var int */
    private $id;

    /**
     * @var int $id
     */
    protected function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return (string)$this->id;
    }
}