<?php

namespace Rodium\Core\Catalog\Api\Variant;

final class RodiumId extends AbstractId
{
    /**
     * @param int $id
     * @return RodiumId
     */
    public static function parse($id)
    {
        return new RodiumId((int)$id);
    }
}