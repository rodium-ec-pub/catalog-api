<?php

namespace Rodium\Core\Catalog\Api\Variant;

final class Symbol
{
    /** @var string */
    private $symbol;

    /**
     * @param string $symbol
     */
    public function __construct($symbol)
    {
        $this->symbol = (string)$symbol;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->symbol;
    }
}